This is a Drupal 10 platform for Aegir 3, including the changes described by
Steven Jones in [this blog
post](https://www.computerminds.co.uk/articles/aegir-3-and-drupal-10-just-about-working).

Authors: Dan Friedman (@lamech) and Scott Zhu Reeves (@star-szr).
